﻿using System;
using System.Diagnostics;
using Prism.Mvvm;

namespace PrismTabNav1.ViewModels
{
    public class TabContainerPageViewModel : BindableBase
    {
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public TabContainerPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabContainerPageViewModel)}:  ctor");
            Title = "Tab Container Page";
        }
    }
}
